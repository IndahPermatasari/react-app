import React from "react";
function ContactCard(props) {
  return (
    <div className="contact-card">
      <img src={props.data.imgUrl}></img>
      <h3>{props.data.name}</h3>
      <p>Phone: {props.data.phone}</p>
      <p>Email: {props.data.email}</p>
    </div>
  );
}
export default ContactCard;
