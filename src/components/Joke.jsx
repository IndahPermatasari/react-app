import React from "react";

function Joke(props) {
  return (
    <div>
      <h6 style={{ display: !props.question && "none" }}>
        Question : {props.question}
      </h6>
      <p style={{ color: !props.question && "#888888" }}>
        Answer : {props.punchLine}
      </p>
    </div>
  );
}
export default Joke;
