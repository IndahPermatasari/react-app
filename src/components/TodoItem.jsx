import React, { Component } from "react";

class TodoItem extends Component {
  render() {
    const { completed, text, id } = this.props.todo;
    const completedStyle = {
      fontStyle: "italic",
      color: "#cdcdcd",
      textDecoration: "line-through"
    };
    return (
      <div className="todo-item">
        <input
          type="checkbox"
          onChange={event => this.props.handleChange(id)}
          checked={completed}
        />
        <p style={completed ? completedStyle : null}>Todo : {text}</p>
      </div>
    );
  }
}

export default TodoItem;
