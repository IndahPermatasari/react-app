import React from "react";
function FormComponent(props) {
  return (
    <main>
      <form className="">
        <input
          placeholder="First name"
          name="firstName"
          onChange={props.handleChange}
          value={props.firstName}
        />
        <br />
        <input
          placeholder="Last name"
          name="lastName"
          onChange={props.handleChange}
          value={props.lastName}
        />
        <br />
        <input
          placeholder="Age"
          name="age"
          value={props.age}
          onChange={props.handleChange}
        />
        <br />
        <label>
          <input
            type="radio"
            name="gender"
            value="male"
            onChange={props.handleChange}
            checked={props.gender === "male"}
          />
          Male
        </label>
        <br />
        <label>
          <input
            type="radio"
            name="gender"
            value="female"
            onChange={props.handleChange}
            checked={props.gender === "female"}
          />
          Female
        </label>
        <br />
        <select
          value={props.location}
          onChange={props.handleChange}
          name="location"
        >
          <option value="">Please Choose location</option>
          <option value="bali">Bali</option>
          <option value="jakarta">Jakarta</option>
          <option value="bandung">Bandung</option>
          <option value="yogjakarta">Yogjakarta</option>
          <option value="ambon">Ambon</option>
        </select>
        <br />
        <label>
          <input
            type="checkbox"
            name="isVegan"
            onChange={props.handleChange}
            checked={props.dietaryRestriction.isVegan}
          />
          Vegan?
        </label>
        <br />
        <label>
          <input
            type="checkbox"
            name="isKosher"
            onChange={props.handleChange}
            checked={props.dietaryRestriction.isKosher}
          />
          Kosher?
        </label>
        <br />
        <label>
          <input
            type="checkbox"
            name="isLactoseFree"
            onChange={props.handleChange}
            checked={props.dietaryRestriction.isLactoseFree}
          />
          Lactose Free?
        </label>
        <br />
        <button>Submit</button>
      </form>
      <br />
      <p>
        Name : {props.firstName} {props.lastName}
      </p>
      <p>Age : {props.age}</p>
      <p>Location : {props.location}</p>
      <p>You are {props.gender}</p>
      <div>
        <p>Vegan : {props.dietaryRestriction.isVegan ? "True" : "False"}</p>
        <p>Kosher : {props.dietaryRestriction.isKosher ? "True" : "False"}</p>
        <p>
          LactoseFree :{" "}
          {props.dietaryRestriction.isLactoseFree ? "True" : "False"}
        </p>
      </div>
    </main>
  );
}
export default FormComponent;
