import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <div>
        <header>
          <img
            src="./../img/c.jpg"
            className="header-image"
            alt="Cat Image"
          ></img>
          <p>Cat Generator</p>
        </header>
      </div>
    );
  }
}
export default Header;
