import React, { Component } from "react";
import FormComponent from "./FormComponent";

class Form extends Component {
  constructor() {
    super();
    this.state = {
      firstName: "",
      lastName: "",
      age: "",
      location: "",
      gender: "",
      dietaryRestriction: {
        isVegan: false,
        isKosher: true,
        isLactoseFree: false
      }
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    const { name, type, value, checked } = event.target;
    if (type === "checkbox") {
      this.setState(prevState => {
        return {
          dietaryRestriction: {
            ...prevState.dietaryRestriction,
            [name]: checked
          }
        };
      });
    } else {
      this.setState({ [name]: value });
    }
  }
  render() {
    return <FormComponent {...this.state} handleChange={this.handleChange} />;
  }
}

export default Form;
