import React, { Component } from "react";
import memeImage from "./../data/memeImageData";

class MemeGenerator extends Component {
  constructor() {
    super();
    this.state = {
      topText: "",
      bottomText: "",
      randomImage: "./../img/a.jpg",
      allMemeImage: []
    };
  }
  componentDidMount() {
    const data = memeImage;
    this.setState({ allMemeImage: data });
  }
  handleChange = event => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };
  handleSubmit = event => {
    event.preventDefault();
    const randNum = Math.floor(Math.random() * this.state.allMemeImage.length);
    const randomImage = this.state.allMemeImage[randNum].image;
    this.setState({ randomImage: randomImage });
  };
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            name="topText"
            placeholder="Top Text"
            value={this.state.topText}
            onChange={this.handleChange}
          />
          <br />
          <input
            type="text"
            name="bottomText"
            placeholder="Bottom Text"
            value={this.state.bottomText}
            onChange={this.handleChange}
          />
          <br />
          <button>Generate</button>
        </form>
        <div className="meme">
          <img src={this.state.randomImage} alt="" />
          <p className="top">{this.state.topText}</p>
          <p className="bottom">{this.state.bottomText}</p>
        </div>
      </div>
    );
  }
}
export default MemeGenerator;
