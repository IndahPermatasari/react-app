import React from "react";
import Header from "./components/Header";
import MemeGenerator from "./components/MemeGenerator";
// import { useSelector, useDispatch } from "react-redux";
import { connect } from "react-redux";
import { fetchUser } from "./store/actions/userAction";
import { increment, decrement } from "./store/actions/counterAction";

function App(props) {
  const { counter, isLogged, increments, decrements, fetchUsers } = props;

  console.log(props);

  return (
    <div className="app">
      <h1>Counter {counter}</h1>
      <button onClick={increments}>+</button>
      <button onClick={decrements}>-</button>
      {isLogged && (
        <h2>Valuable Information that cannot be shown if not Logged In</h2>
      )}
      <br />
      <p>Users : </p>
      {props.users.length === 0 ? (
        <p>User Not Found</p>
      ) : (
        props.users.map(user => (
          <p key={user.id}>
            {user.id} - {user.name}
          </p>
        ))
      )}
      <button onClick={fetchUsers}>Fetch Users</button>
    </div>
  );
}
const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = dispatch => {
  return {
    increments: () => dispatch(increment()),
    decrements: () => dispatch(decrement()),
    fetchUsers: () => dispatch(fetchUser())
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
