export const fetchUser = () => {
  return async function(dispatch) {
    await fetch("https://jsonplaceholder.typicode.com/users")
      .then(res => res.json())
      .then(res => dispatch({ type: "FETCH_USER", payload: res }));
  };
};
