import counterReducer from "./counter";
import isLoggedReducer from "./isLogged";
import { combineReducers } from "redux";
import userReducers from "./users";

const allReducers = combineReducers({
  counter: counterReducer,
  isLogged: isLoggedReducer,
  users: userReducers
});
export default allReducers;
